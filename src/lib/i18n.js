import React from 'react';
import i18n from 'i18next';
import {useTranslation, initReactI18next} from 'react-i18next';

const en = {
  home: {
    welcome: 'Welcome to localization',
  },
  setting: {
    title: 'Setting',
  },
};

const kh = {
  home: {
    welcome: 'ស្វាគមន៍មកកាន់',
  },
  setting: {
    title: 'កំណត់',
  },
};

i18n.use(initReactI18next).init({
  resources: {
    en,
    kh,
  },
  lng: 'en',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;

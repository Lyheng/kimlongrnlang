import {createDrawerNavigator} from 'react-navigation-drawer';
import Home from './Home';
import {createAppContainer} from 'react-navigation';
import Setting from './Setting/index';
import {createStackNavigator} from 'react-navigation-stack';
const Stack = createStackNavigator({
  Home,
});
const Drawer = createDrawerNavigator({
  Home,
  Setting,
  Stack,
});

const MyApp = createAppContainer(Drawer);

export default MyApp;

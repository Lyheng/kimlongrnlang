import React, {useState, useEffect} from 'react';
import {Text, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {useTranslation} from 'react-i18next';
import AsyncStorage from '@react-native-community/async-storage';
const Home = () => {
  const {t, i18n} = useTranslation();
  let isChange = false;

  //   useEffect(() => {
  //     AsyncStorage.getItem('lang').then(asyncLang => {
  //       const lang = isChange ? 'en' : 'kh';
  //       if (lang === asyncLang) {
  //         console.log('set lang ', lang, isChange);
  //         AsyncStorage.setItem('lang', lang);
  //         i18n.changeLanguage(lang);
  //       } else {
  //         i18n.changeLanguage(asyncLang);
  //       }
  //     });
  //   }, [isChange]);
  return (
    <SafeAreaView>
      <Text>{t('home:welcome')}</Text>

      <View
        style={{height: 200, justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          onPress={async () => {
            i18n.changeLanguage('en');
            AsyncStorage.setItem('lang', 'en');
          }}
          style={{
            width: 150,
            height: 50,
            backgroundColor: 'green',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white'}}>EN</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={async () => {
            i18n.changeLanguage('kh');
            AsyncStorage.setItem('lang', 'kh');
          }}
          style={{
            width: 150,
            height: 50,
            backgroundColor: 'green',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white'}}>Kh</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default Home;

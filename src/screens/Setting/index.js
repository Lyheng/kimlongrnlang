import React, {useEffect} from 'react';
import {View, TouchableOpacity, Text, SafeAreaView} from 'react-native';
import i18n from '../../lib/i18n';
import i18next from 'i18next';

const Setting = ({navigation}) => {
  console.log('navigation ===>', navigation);

  return (
    <SafeAreaView>
      <View
        style={{
          height: 100,
          justifyContent: 'center',
          alignItems: 'center',
          width: '100%',
        }}>
        <TouchableOpacity
          onPress={async () => {
            i18n.changeLanguage('en');
            AsyncStorage.setItem('lang', 'en');
          }}
          style={{
            width: 150,
            height: 50,
            backgroundColor: 'green',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white'}}>EN</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={async () => {
            i18n.changeLanguage('kh');
            AsyncStorage.setItem('lang', 'kh');
          }}
          style={{
            width: 150,
            height: 50,
            backgroundColor: 'green',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white'}}>Kh</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

Setting.navigationOptions = value => {
  console.log('vlaue vlaue===>', value);
  let title = i18next.t('setting:title');
  console.log('this is title ', title);
  return {
    title,
  };
};

export default Setting;
